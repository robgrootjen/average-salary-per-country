The datapackage shows the average salary per country since 2000

## Data
The data is sourced from: 
* https://en.wikipedia.org/wiki/List_of_countries_by_average_wage

The repo includes:
* datapackage.json
* mmonthlywage.csv
* yearlywage.csv
* process.py

--------------------------------------------------------------------------------------------------------------------------------

## Preparation

Requires:
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***monthlywage.csv*** 
* The CSV Data has 2 colummns. 
    * Country
    * Average Monthly wage 2015 in $

***yearlywage.csv***
* The CSV Data includes 11 columns
    * Country
    * 2000
    * 2005
    * 2010
    * 2011
    * 2012
    * 2013
    * 2014
    * 2015
    * 2016
    * 2017

***datapackage.json***
* The json file has all the information from the two csv files, licence, author and includes 2 line graphs.

***process.py***
* The script will scrape the 2 tables from - https://en.wikipedia.org/wiki/List_of_countries_by_average_wage
    
***Instructions:***
* Copy process.py script in the "Process" folder.
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* 2 CSV´s file will be saved in document where your terminal is at the moment.
----------------------------------------------------------------------------------------------------------------------------------------

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 
