import requests
from bs4 import BeautifulSoup
import csv

#####################################################################################################

#Get content
result1 = requests.get('https://en.wikipedia.org/wiki/List_of_countries_by_average_wage')

####################################################################################################

#Save source in variable
src1 = result1.content

###################################################################################################

#Activate soup
soup = BeautifulSoup(src1,'lxml')

##################################################################################################

#Look for tables
findtables = soup.findAll('table')

#################################################################################################

#Look for table1 and save in csv
table1 = findtables[8]
with open('yearlywage.csv','w',newline='') as f:
    writer = csv.writer(f)
    for tr in table1('tr'):
        row = [t.get_text(strip=True)for t in tr (['td','th'])]
        cell1 = row[0]
        cell2 = row[1].replace(',','')
        cell3 = row[2].replace(',','')
        cell4 = row[3].replace(',','')
        cell5 = row[4].replace(',','')
        cell6 = row[5].replace(',','')
        cell7 = row[6].replace(',','')
        cell8 = row[7].replace(',','')
        cell9 = row[8].replace(',','')
        cell10 = row[9].replace(',','')
        cell11 = row[10].replace(',','')
        row = [cell1,cell2,cell3,cell4,cell5,cell6,cell7,cell8,cell9,cell10,cell11]
        writer.writerow(row)

####################################################################################################

#Look for table2 and save in csv
table2 = findtables[9]
with open('monthlywage.csv','w',newline = '') as f:
    writer = csv.writer(f)
    writer.writerow(('Country','Average Monthly Wage 2015 in $'))
    for tr in table2('tr')[1:]:
        row = [t.get_text(strip=True)for t in tr (['td','th'])]
        cell1 = row[0]
        cell2 = row[1].replace(',','').replace('$','')
        row = [cell1,cell2]
        writer.writerow(row)

#####################################################################################################